from setuptools import setup

setup(name='blockchain',
      version='0.0.0',
      description='A silly blockchain',
      packages=['blockchain'],
      install_require=[],
      setup_requires=[
        'pytest-runner',
          ],
      tests_require=['pytest'])

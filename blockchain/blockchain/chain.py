"""Chain definition for blockchain."""
from blockchain.block import Block


class Chain:
    """Class representing the blockchain."""

    def __init__(self, zeroes):
        """Construct chain with sentry block."""
        self.zeroes = zeroes
        # sentry block
        block = Block(0, "0", 0, "SENTRY")
        self.blocks = []
        self.blocks.append(block)
        self.curr_index = 0
        self.prev_hash = ""
        self.curr_hash = block.hash()

    def add(self, block):
        """Add block with nonce and encrypted_data to the chain."""
        self.curr_index += 1
        self.prev_hash = self.curr_hash
        self.curr_hash = block.hash()
        self.blocks.append(block)

    def get_blocks(self):
        """Return all the blocks in the chain."""
        return self.blocks

    def replace(self, chain):
        """Replace self with different chain."""
        self.blocks = chain.blocks
        self.curr_index = len(self.blocks) - 1
        self.curr_hash = self.blocks[-1].hash()
        self.prev_hash = self.blocks[-2].hash()

    def mine(self, encrypted_data):
        """Mine for next valid hash in chain to insert block."""
        nonce = 1

        while True:
            block = Block(nonce, self.curr_hash, self.curr_index + 1, encrypted_data)
            nonce_hash = block.hash()
            # If zeroes were say 3, then "0" * 3 => "000"
            if nonce_hash[:self.zeroes] == "0" * self.zeroes:
                self.add(block)
                return nonce
            nonce += 1
        return nonce

    def is_valid(self):
        """Make sure that chain of hashes are valid."""
        prev_block = self.blocks[0]
        for block in self.blocks[1:]:
            if prev_block.hash() != block.get_prev_hash():
                return False
            nonce_hash = block.hash()
            if nonce_hash[:self.zeroes] != "0" * self.zeroes:
                return False
            prev_block = block
        return True

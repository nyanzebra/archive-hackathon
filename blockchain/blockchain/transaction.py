"""Transaction definition for blockchain data."""
import json


class Transaction:  # pylint: disable=too-few-public-methods
    """Class representing the transaction."""
    def __init__(self, sender, receiver, amount):
        """ Construct transaction with given data """
        self.sender = sender
        self.receiver = receiver
        self.amount = amount

    def to_json(self):
        """Convert transaction into JSON."""
        return json.dumps(self.__dict__, sort_keys=True)

"""Block definition for blockchain."""
import hashlib
import json


class Block:
    """Class representing the individual block in a blockchain."""

    def __init__(self, nonce, prev_hash, index, encrypted_data):
        """Construct a new block."""
        self.nonce = nonce
        self.prev_hash = prev_hash
        self.index = index
        self.encrypted_data = encrypted_data

    def get_prev_hash(self):
        """Return previous hash that should match hash of previous block."""
        return self.prev_hash

    def get_index(self):
        """Return index of block in chain."""
        return self.index

    def get_encrypted_data(self):
        """Return encrypted data in the block."""
        return self.encrypted_data

    def get_nonce(self):
        """Return nonce for block."""
        return self.nonce

    def to_json(self):
        """Convert block into JSON."""
        return json.dumps(self.__dict__, sort_keys=True)

    def hash(self):
        """Calculate hash over block."""
        return hashlib.sha256(self.to_json().encode()).hexdigest()

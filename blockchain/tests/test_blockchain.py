import copy
import json
import hashlib

from blockchain import Block, Chain, Transaction

class TestBlockChain(object):

    def test_json_block(self):
        block = Block(1, "0", 0, "Test Data")
        actual = block.to_json()
        expected = json.dumps(block.__dict__, sort_keys=True)
        assert actual == expected

    def test_make_chain(self):
        chain = Chain(1)
        transaction1 = Transaction("Foo", "Bar", 100)
        encrypted_data = hashlib.sha256(str(transaction1.to_json()).encode()).hexdigest()
        chain.mine(encrypted_data)
        assert chain.is_valid(), True

        bad_chain = copy.deepcopy(chain)
        bad_chain.add(Block(-1, 0, 0, "Test 2"))
        assert bad_chain.is_valid() == False

        transaction2 = Transaction("Foo", "Bar", 50)
        encrypted_data = hashlib.sha256(str(transaction2.to_json()).encode()).hexdigest()
        chain.mine(encrypted_data)
        assert chain.is_valid() == True

        bad_chain.replace(chain)
        assert bad_chain.is_valid() == True
